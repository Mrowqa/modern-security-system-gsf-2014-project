DO_PRINT_MESSAGE = True
ALARM_MESSAGE = "Alarm detected at: $(date +%d/%b/%Y\ %X)"


DO_PLAY_SOUND = True
ALARM_SOUND_FILENAME = "assets/alarm.ogg"


DO_SEND_SMS = False
SMS_DATA = [
# DO ***NOT*** USE TAB! It's special character, so sms limit is reduced from 160 to 70 characters!
	{ 'to': '662736516',
		'content': """Break-in attempt at $(date +%d/%b/%Y\ %X)!

		You can now call the police or just panic!""" }
# Info: each line will be stripped, so you can use white characters at the begin and end of them.
]


DO_SEND_MAILS = False
MAILS_DATA = [
    { 'to': 'foxers.team@gmail.com',
        'subject': 'ALARM!',
        'content': 'break-in suspicion!' },
    { 'to': 'foxers.team@gmail.com',
        'subject': 'Break-in attempt at $(date +%d/%b/%Y\ %X)',
        'content': """Hi!
        
        More info you can find below:
        
        $(cat /var/log/alarm-center.log)""" }
# Info: each line will be stripped, so you can use white characters at the begin and end of them.
]


# Not implemented yet
DO_SEND_VMS = False
VMS_DATA = [
	{ 'to': '662736516',
		'text': 'Hello! This text will be read by speech synthesizer.' },
	{ 'to': '662736516',
		'recording': 'assets/phone_call_alarm_info.ogg' }
]
