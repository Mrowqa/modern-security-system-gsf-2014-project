#!/bin/bash

INSTALL_PATH=/usr/local/alarm-center # if you change this, change $CHDIR in ./alarm-center, too
SERVICE_SCRIPT_DIR=/etc/init.d


if [ $(id -u) -ne 0 ]; then
	echo "Please run as root."
	exit 1
fi


cd $(dirname $0)
mkdir -p $INSTALL_PATH
cp -R ./service_files/* $INSTALL_PATH
cp ./alarm-center $SERVICE_SCRIPT_DIR
chmod 755 $SERVICE_SCRIPT_DIR/alarm-center
chown root $SERVICE_SCRIPT_DIR/alarm-center

echo Installed in: $INSTALL_PATH
echo Please see there configuration files config/ subdirectory.
echo Now you can use \"service alarm-center\" command.