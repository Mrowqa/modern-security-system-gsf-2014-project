#!/bin/sh
LIB_DIR=$(dirname $0)/extlib

export BITIFY_API_HOME=$LIB_DIR/raspi-bitify/i2c-sensors
export SMSAPI_HOME=$LIB_DIR/smsapi-python-client
export BOTTLE_API_HOME=$LIB_DIR/bottlepy

export PYTHONPATH=$PYTHONPATH:$BITIFY_API_HOME:$SMSAPI_HOME:$BOTTLE_API_HOME
