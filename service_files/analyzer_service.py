#!/usr/bin/python

import math
import numpy
import time

from config.prober import ALARM_COOLDOWN
 

class AnalyzerService:

	def __init__(self, sampling_frequency = None):
		self.set_sampling_frequency(sampling_frequency)
		self.set_notification_callback(lambda: None)
		self.alarm_timeout = ALARM_COOLDOWN # seconds
		self.__last_alarm = time.time() - self.alarm_timeout
	
	def analyze(self, new_data):
		# do magic
		if self.__sampling_frequency == None: return # not initialized
		#print("Analyzing data: " + str(new_data))
		
		# TODO do analyze in new thread!
		v3len = lambda x: math.sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]) # length of 3D vector
		new_data = [v3len(i) for i in new_data]
		self.__samples_history = new_data + self.__samples_history
		if len(self.__samples_history) < self.__needed_samples:
			return # too small samples
		self.__samples_history = self.__samples_history[:self.__needed_samples]
		
		# last alarm still last
		if not self.__check_if_last_alarm_timeouted():
			return
		
		avg = numpy.mean(self.__samples_history)
		over2 = [i for i in self.__samples_history if i > 2*avg]
		over5 = [i for i in self.__samples_history if i > 5*avg]
		
		percent2 = len(over2) / float(self.__needed_samples)
		percent5 = len(over5) / float(self.__needed_samples)
		
		needed_pnts = 100.0
		pnt2 = (100 * percent2) * (needed_pnts / 15)
		pnt5 = (100 * percent5) * (needed_pnts / 7)
		
		if pnt2 + pnt5 > needed_pnts: # ALARM!
			self.try_call_alarm()
	
	def set_sampling_frequency(self, sampling_frequency):
		self.__sampling_frequency = sampling_frequency
		if self.__sampling_frequency == None: return # not initialized
		self.__samples_history = []
		self.__needed_samples = int(math.ceil(sampling_frequency * 5))
	
	def get_sampling_frequency(self):
		return self.__sampling_frequency # multithreading? => mutex!
	
	def set_notification_callback(self, callback):
		self.__notif_callback = callback
	
	def reset(self):
		self.__samples_history = []
		
	def __check_if_last_alarm_timeouted(self):
		return not time.time() < self.__last_alarm + self.alarm_timeout

	def try_call_alarm(self):
		if self.__check_if_last_alarm_timeouted():
			self.__last_alarm = time.time()
			self.__notif_callback()
			