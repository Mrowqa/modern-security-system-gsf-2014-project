#!/usr/bin/python

from threading import Thread
import time

class NotificationManager:
	def __init__(self):
		self.clear_notifiers()
		self.use_separate_threads = False
	
	def notify(self):
		for method, args in self.__notifiers.itervalues():
			if self.use_separate_threads:
				Thread(name='NotificationManger.notify() slave thread', target=method, args=args).start()
			else:
				method(*args)
	
	def add_notifier(self, method, *args):
		self.__notifiers[self.__id_cnt] = (method, args)
		self.__id_cnt += 1
	
	def get_all_notifiers(self):
		return dict(self.__notifiers)
	
	def clear_notifiers(self):
		self.__id_cnt = 0
		self.__notifiers = {}
	
	def delete_notifier(self, notifier_id):
		if self.__notifiers.has_key(notifier_id):
			del self.__notifiers[notifier_id]
		else:
			print("NotificationManager warning: attempt to delete non-existing notifier, id=%s" % str(notifier_id))
