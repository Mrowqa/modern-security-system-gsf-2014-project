#!/usr/bin/python
from threading import Thread, Lock
from time import time, sleep
import math

from smbus import SMBus
from bitify.python.sensors.hmc5883l import HMC5883L
from bitify.python.utils.i2cutils import i2c_raspberry_pi_bus_number

from analyzer_service import AnalyzerService
from config.prober import DISCONECTED_SENSOR_TRIGGERS_ALARM


class ProberServer:
	
	SAMPLE_RATE =	{ k: v for k, v in HMC5883L.SAMPLE_RATE.iteritems() if v > 0 }
	GAIN_SCALE =	{ k: v[0] for k, v in HMC5883L.GAIN_SCALE.iteritems() }

	def __init__(self, sensor_offsets = [0, 0, 0]):
		self.__sensor_offsets = sensor_offsets
		self.__analyzer = AnalyzerService()
		self.__worker = ProberServer.__ProberWorkerThread(self.__analyzer)
	
	def start(self, sample_rate_mode=4, history_length=60.0, gain_scale=1): # TODO Refactorization: move validation from HttpServer here, error handling through exceptions
		self.stop()
		self.__analyzer.set_sampling_frequency(self.SAMPLE_RATE[sample_rate_mode])
		self.__worker = ProberServer.__ProberWorkerThread(self.__analyzer, sample_rate_mode, history_length, gain_scale, self.__sensor_offsets)
		self.__worker.start()
		
	def stop(self):
		self.__worker.stop()
	
	def is_running(self):
		return self.__worker.is_alive()
	
	def get_configuration(self):
		return self.__worker.get_configuration()

	def get_measurement_results(self):
		return self.__worker.get_measurement_results()
	
	def set_notification_callback(self, callback):
		self.__analyzer.set_notification_callback(callback)
	
	
	class __ProberWorkerThread(Thread):
		def __init__(self, analyzator=None, sample_rate_mode=4, history_length=60.0, gain_scale=1, sensor_offsets=[0,0,0]):
			super(self.__class__, self).__init__(name="Prober Server worker - data reader")
			self.daemon = True
			
			self.__analyzer = analyzator
			self.__analyze_timestamp = 1.0 # seconds
			self.__analyze_awaiting_samples = 0
			
			self.__is_sensor_connected = False
			self.__should_be_running = False
			self.__results_read_lock = Lock() # threading.Lock()
			self.__sensor_offsets = sensor_offsets
			self.__init_device(False)
			if not self.__is_sensor_connected:
				self.__sensor = self.__sensor_mock()
			self.__set_configuration(sample_rate_mode, history_length, gain_scale)
			
		def __init_device(self, restore_saved_configuration = True):
			self.__analyzer.reset()
			try:
				bus_no = SMBus(i2c_raspberry_pi_bus_number())
				sensor_address = 0x1e
				sensor_name = "HMC5883L"
				self.__sensor = HMC5883L(bus_no, sensor_address, sensor_name)
				self.__sensor.set_offsets(*self.__sensor_offsets)
				if restore_saved_configuration:
					self.__set_configuration(self.__sample_rate_mode, self.__history_length, self.__gain_scale)
				self.__is_sensor_connected = True
			except IOError:
				self.__is_sensor_connected = False
		
		def stop(self):
			self.__should_be_running = False
			try:					self.join()
			except RuntimeError:	pass
			
		def run(self):
			self.__should_be_running = True
			last_update = time()
			while self.__should_be_running:
				try:
					if not self.__is_sensor_connected:
						self.__init_device()
					self.__sensor.read_raw_data()
					x, y, z = self.__sensor.read_scaled_x(), self.__sensor.read_scaled_y(), self.__sensor.read_scaled_z()
				except IOError:
					x, y, z = 0, 0, 0
					self.__is_sensor_connected = False
					if DISCONECTED_SENSOR_TRIGGERS_ALARM:
						self.__analyzer.try_call_alarm()
				
				
				#--------------
				act_as_field_change = False
				if act_as_field_change:
					max_value = ProberServer.GAIN_SCALE[self.__gain_scale] * 1000
					if abs(x) >= max_value or abs(y) >= max_value or abs(z) >= max_value:
						x, y, z = 0, 0, 0
					
					ox, oy, oz = self.__measurement_history[0]
					x += ox
					y += oy
					z += oz
				#--------------
				
				data_to_analyze = None
				self.__analyze_awaiting_samples += 1
				with self.__results_read_lock:
					self.__measurement_history.pop()
					self.__measurement_history.insert(0, [x, y, z])
					if self.__analyze_awaiting_samples / self.__reading_frequency > self.__analyze_timestamp:
						data_to_analyze = self.__measurement_history[:self.__analyze_awaiting_samples]
						self.__analyze_awaiting_samples = 0
				if data_to_analyze != None:
					self.__analyzer.analyze(data_to_analyze)
				
				sleep_time = max(1.0 / self.__reading_frequency - (time() - last_update), 0.0)
				sleep(sleep_time)
				last_update = time()
		
		def __set_configuration(self, sample_rate_mode, history_length, gain_scale):
			if not sample_rate_mode in ProberServer.SAMPLE_RATE or not gain_scale in ProberServer.GAIN_SCALE:
				return
			self.__sample_rate_mode = sample_rate_mode
			self.__gain_scale = gain_scale
			self.__sensor.set_sampling(sample_rate_mode)
			self.__sensor.set_gain(gain_scale)
			
			self.__reading_frequency = HMC5883L.SAMPLE_RATE[sample_rate_mode];
			self.__history_length = history_length
			self.__measurement_history = [[0,0,0]] * int(math.ceil(self.__reading_frequency * self.__history_length))
		
		def get_configuration(self):
			return {'sample rate mode':		self.__sample_rate_mode,
					'gain scale':			self.__gain_scale,
					'reading frequency':	self.__reading_frequency,
					'history length':		self.__history_length,
					'sensor offsets':		self.__sensor_offsets,
					'is connected':			"yes" if self.__is_sensor_connected else "no"
				}
				
		def get_measurement_results(self):
			with self.__results_read_lock:
				return list(self.__measurement_history)
				
		#--------------- sensor mock (replacement used during failure on the original) -----------
		class __sensor_mock:
			def set_sampling(*args):	pass
			def set_gain(*args):		pass
			def set_offsets(*args):		pass
			def read_raw_data(*args):	raise IOError("Sensor not connected!")
			def read_scaled_x(*args):	raise IOError("Sensor not connected!")
			def read_scaled_y(*args):	raise IOError("Sensor not connected!")
			def read_scaled_z(*args):	raise IOError("Sensor not connected!")
