google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(updateLiveData);

var chart = null;

var xmlhttp;
if(window.XMLHttpRequest)	// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp=new XMLHttpRequest();
else // code for IE6, IE5
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");

var gain_scale_selection = document.getElementById('gain_scale');
var gs_option = gain_scale_selection.options[gain_scale_selection.selectedIndex].text;
var gs_value = parseFloat(gs_option.split(' ')[1]) * 1000;


var mutexLocked = false;
function __updater() {
	if (xmlhttp.readyState!=4 || xmlhttp.status!=200)
		return;
	
	try {
		var raw_data = JSON.parse(xmlhttp.responseText);
		
		var prepared_data = [['Time', 'X axis', 'Y axis', 'Z axis']];
		var time_step = 1/raw_data['frequency'];
		for(var i=0; i<raw_data['content'].length; i++) {
			var result = raw_data['content'][i];
			var label_value = parseFloat((i*time_step).toFixed(3));
			var entry = [label_value].concat(result)
			prepared_data.push(entry);
		}
		
		var data = google.visualization.arrayToDataTable(prepared_data);

		var options = {
			title: 'Axes values [mGs]',
			hAxis: {
				title: 'Measurement history [sec]', 
				titleTextStyle: {color: '#333'},
				direction: -1
			},
			colors: ['red', 'green', 'blue'],
			lineWidth: 1,
			animation: {
				duration: 0,
				easing: 'linear'
			}
		};
		if (use_default_chart_scale)
			options['vAxis'] = {
				minValue: -gs_value, // gain scale
				maxValue: gs_value
			};

		if(chart == null)
			chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
		chart.draw(data, options);
			
		var status_elem = document.getElementById('device_status');
		switch(raw_data['is_connected']) {
			case "yes":
				status_elem.innerHTML = "connected";
				status_elem.style.color = "green";
				break;
					
			case "no":
				status_elem.innerHTML = "disconnected";
				status_elem.style.color = "red";
				break;
				
			default:
				status_elem.innerHTML = "unknown";
				status_elem.style.color = "blue";
				break;
		}
		
		var alarm_div = document.getElementById('last_alarm_div');
		var alarm_span = document.getElementById('last_alarm_info');
		if(raw_data['last_alarm_at'] != alarm_span.innerHTML) {
			alarm_span.innerHTML = raw_data['last_alarm_at'];
			
			if(last_alarm_timeout_id != null) {
				alarm_div.className = "alert alert-danger";
				clearTimeout(last_alarm_timeout_id);
				
				if(use_alert_on_alarm)
					alert("Detected alarm!");
			}
			
			last_alarm_timeout_id = setTimeout(function() {
				alarm_div.className = "";
			}, 10000);
		}
	}
	catch(exception) {
		console.log("updateLiveData/__update(): Exception: " + exception);
	}
	mutexLocked = false;
}
xmlhttp.onreadystatechange = __updater;

function updateLiveData() {
	if(mutexLocked == true)
		return;
	mutexLocked = true;
	
	clearTimeout(update_timeout_id)
	update_timeout_id = setTimeout(function() {
		xmlhttp.abort();
		mutexLocked = false;
	}, update_interval * 2);
	
	try {
		xmlhttp.open("GET", "/measurement_results.json", true);
		xmlhttp.send();
	}
	catch(exception) {
		console.log("updateLiveData(): Exception: " + exception);
		mutexLocked = false;
	}
}

function updateClientSettings() {
	update_interval = document.getElementById('refresh_interval_input').value; // client-side validation via input type
	clearInterval(refresh_handle);
	refresh_handle = setInterval(updateLiveData, update_interval);
	
	use_default_chart_scale = document.getElementById('use_default_scale_input').checked;
	use_alert_on_alarm = document.getElementById('use_alert_input').checked;
	
	document.getElementById('applying_button').value = "Applied!";
	setTimeout(function() {
		document.getElementById('applying_button').value = "Apply";
	}, 1000);
}

var update_timeout_id = null;
var last_alarm_timeout_id = null;
var update_interval = document.getElementById('refresh_interval_input').value;
var use_default_chart_scale = document.getElementById('use_default_scale_input').checked;
var use_alert_on_alarm = document.getElementById('use_alert_input').checked;
var refresh_handle = setInterval(updateLiveData, update_interval);