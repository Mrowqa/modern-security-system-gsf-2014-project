#!/usr/bin/python
from bottle import Bottle, run, redirect, request, template, TEMPLATE_PATH, static_file
import re
import subprocess
#import bitify.python.utils.calibrate_hmc5883l

from prober_server import ProberServer
from notification_manager import NotificationManager
from notifications import Notifications
from config.notif_alarm import DO_PLAY_SOUND, ALARM_SOUND_FILENAME, DO_SEND_MAILS, MAILS_DATA, DO_PRINT_MESSAGE, ALARM_MESSAGE, DO_SEND_SMS, SMS_DATA

from config.prober import FREQUENCY_SAMPLE_RATE, GAIN_SCALE, MEASUREMENT_HISTORY_LENGTH

class HttpServer:
	def __init__(self):
		global TEMPLATE_PATH
		
		self.__content_dir = './http_server_content/'
		TEMPLATE_PATH += [self.__content_dir + 'templates/']
		
		self.__prober = ProberServer()
		self.__app = Bottle()
		
		self.__def_routes()
		self.__load_notifications()
	
	
	def __load_notifications(self):
		def process_content(content):
			shell_cmd_call_pattern = r"(?:\\\\)*(\$\((([^\)\\]|\\[^\)]|(?<!\\)(\\\\)*\\\))+)\))"
				# groups: 1st - desired match, 2nd - brackets content
			
			whole_group = 1
			content_group = 2
			
			new_content = ""
			last_pos = 0
			for match in re.finditer(shell_cmd_call_pattern, content):
				new_content += content[last_pos : match.start(whole_group)]
				new_content += subprocess.check_output(content[match.start(content_group) : match.end(content_group)],
					stderr=subprocess.STDOUT, shell=True).strip()
				last_pos = match.end(whole_group)
			new_content += content[last_pos:]
			return new_content
		#--------------------------- end of local def ---------------------
		
		self.__notifs_manager = NotificationManager()
		self.__notifs_manager.use_separate_threads = True
		self.__last_alarm_at = "Never";
		self.__prober.set_notification_callback(self.__notifs_manager.notify)
		
		def update_last_alarm_time():
			self.__last_alarm_at = process_content("$(date +%d/%b/%Y\ %X)")
		self.__notifs_manager.add_notifier(update_last_alarm_time)
				
		if DO_PRINT_MESSAGE:
			self.__notifs_manager.add_notifier(lambda: Notifications.log_to_stdout(process_content(ALARM_MESSAGE)))
		if DO_PLAY_SOUND:
			self.__notifs_manager.add_notifier(Notifications.play_sound, ALARM_SOUND_FILENAME)
		if DO_SEND_SMS:
			def send_all_sms():
				for sms in SMS_DATA:
					to, content = sms['to'], process_content(sms['content'])
					content = '\n'.join([line.strip() for line in content.split('\n')])
					print("Sending sms to: %s, first bytes of content: %s%s" % (to, content[:50].replace('\n', '\\n'), "(...)" if len(content) > 50 else ""))
					Notifications.send_sms(to, content)
			self.__notifs_manager.add_notifier(send_all_sms)
		if DO_SEND_MAILS:
			def send_all_emails():
				for mail in MAILS_DATA:
					to, subject, content = mail['to'], process_content(mail['subject']), process_content(mail['content'])
					content = '\n'.join([line.strip() for line in content.split('\n')])
					print("Sending email to: %s, subject: %s" % (to, subject))
					Notifications.send_email(to, subject, content)
			self.__notifs_manager.add_notifier(send_all_emails)
		
	def __def_routes(self):
		@self.__app.route('/')
		@self.__app.route('/index')
		@self.__app.route('/index.html')
		def index():
			prober_configartion = self.__prober.get_configuration()
			
			errors = index.errors
			sample_rate_mode = prober_configartion['sample rate mode']
			history_length = prober_configartion['history length']
			gain_scale = prober_configartion['gain scale']
			
			index.errors = []
			return template('index.html', sample_rate_mode=sample_rate_mode, history_length=history_length, gain_scale=gain_scale, \
				errors=errors, rate_modes=ProberServer.SAMPLE_RATE, gain_scales=ProberServer.GAIN_SCALE)
		index.errors=[]
			
		@self.__app.route('/', method='POST')
		@self.__app.route('/index', method='POST')
		@self.__app.route('/index.html', method='POST')
		def configure():
			try:
				sample_rate_mode = int(request.forms.get('sample_rate_mode'))
				gain_scale = int(request.forms.get('gain_scale'))
				history_len = float(request.forms.get('history_length').replace(',', '.'))
				if not sample_rate_mode in ProberServer.SAMPLE_RATE:
					index.errors += ["Please choose measurement frequency from the given list!"]
				if not gain_scale in ProberServer.GAIN_SCALE:
					index.errors += ["Please choose sensor field range from the given list!"]
				if history_len < 1:
					index.errors += ["Measurement length history must be greater or equal 1 second!"]
			except ValueError:
				index.errors += ["Measurement frequency, history length and sensor field range must be numbers!"]
				
			if len(index.errors) == 0:
				print("Setting prober configuration to:\n" \
					"\tsample rate: %d (%.2f Hz)\n" \
					"\tgain scale: %d (%.2f Ga)\n" \
					"\thistory length: %.3f sec" \
					% (sample_rate_mode, ProberServer.SAMPLE_RATE[sample_rate_mode], gain_scale, ProberServer.GAIN_SCALE[gain_scale], history_len))
				self.__prober.start(sample_rate_mode, history_len, gain_scale)
			redirect('/')

		@self.__app.route('/measurement_results.json')
		def measurement_results():
			prober_configartion = self.__prober.get_configuration()
			frequency = prober_configartion['reading frequency']
			history_length = prober_configartion['history length']
			is_connected = prober_configartion['is connected']
			last_alarm = self.__last_alarm_at
			
			return '{"frequency": %f, "history_length": %f, "is_connected": "%s", "last_alarm_at": "%s", "content": %s }' % (frequency, history_length, is_connected, last_alarm, str(self.__prober.get_measurement_results()))
		
		@self.__app.route('/assets/<filename:path>')
		def server_assets(filename):
			return static_file(filename, root=self.__content_dir)
	
	
	def start(self):
		config_error = False
		if not FREQUENCY_SAMPLE_RATE in ProberServer.SAMPLE_RATE:
			print("Invalid config: frequency sample rate = " + str(FREQUENCY_SAMPLE_RATE))
			config_error = True
		if not GAIN_SCALE in ProberServer.GAIN_SCALE:
			print("Invalid config: gain scale = " + str(GAIN_SCALE))
			config_error = True
		if MEASUREMENT_HISTORY_LENGTH < 1:
			print("Invalid config: measurement history length = " + str(MEASUREMENT_HISTORY_LENGTH))
			config_error = True
		
		if config_error:
			print("Exiting.")
			exit(1)
			
		self.__prober.start(FREQUENCY_SAMPLE_RATE, MEASUREMENT_HISTORY_LENGTH, GAIN_SCALE)
		run(self.__app, host='0.0.0.0', port=8080)


if __name__ == "__main__":
	server = HttpServer()
	server.start()