Modern security system
======================

GSF project (polish only): <http://goo.gl/24I2oc>

Project was done using Raspberry Pi and HMC5883L magnetometer. Due to that script is written in Python
it's simple to port it to another platforms and use other sensors - you have to just write little class
for communication beetwen OS and magnetometer.

#Features:

* used Python (cross-platfom),
* management via simply config files and webpanel,
* webpanel with real-time chart displaying data from sensor,
* notify alarm via email, sound alarm, SMS,
* installable as service in systems using init.d (e.g. Debian).

#Configuring

Config files can be found in [/service_files/config](modern-security-system-gsf-2014-project/src/master/service_files/config)
directory. After service installation you will be notified when the config files are
located (see [install.sh](modern-security-system-gsf-2014-project/src/master/install.sh)).

#Using

You can run this project in two ways:

1. *Portable mode.* Just run this script:

		service_files/start.sh

2. *Using as service.* Install service:

		sudo ./install.sh
		
	Then you can run service by:
	
		sudo service alarm-center start
		
	And make it starting when OS boots:
	
		sudo update-rc.d alarm-center defaults

#Updating

If you cloned repository with git, you can download new changes by running

	git pull
	
#Logs

Log file is located in */var/log/alarm-center.log*. Log is not rotated or truncated by default,
so it would be good to do some clean up once in a while.

#License

All the code excluding extlib is licenced under [GNU GPL v3 license](http://www.gnu.org/copyleft/gpl.html).

External libraries' licences:

* bottlepy - [MIT License](modern-security-system-gsf-2014-project/src/master/service_files/extlib/bottlepy/LICENSE)
* raspi-bitify library - [Apache License](modern-security-system-gsf-2014-project/src/master/service_files/extlib/raspi-bitify/LICENSE.md)
* SMSAPI python client - [Apache License](modern-security-system-gsf-2014-project/src/master/service_files/extlib/smsapi-python-client/LICENSE)


#TODO:

* use webpy instead bottlepy (more readable code)
* implement VMS (phone call) notification (SMSAPI provides that, you just have to find in documentation)
* move data validation from HttpServer to ProberServer (POST HTTP method on index.html)
* AnalyzerService should analyze data in slave thread
* move default client configuration from index.html template to configuration file
* create webpanel for configuring notification methods 
* improve AnalyzerService algorithm
* open issues in tracker and delete this TODO




Have fun!




Copyright (C) 2014 Artur "Mrowqa" Jamro, <http://mrowqa.pl>