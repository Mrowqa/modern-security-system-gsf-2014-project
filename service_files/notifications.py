#!/usr/bin/python

from config.notif_accounts import MAIL_SERVER, MAIL_LOGINNAME, MAIL_PASSWORD, MAIL_FROM, SMSAPI_LOGINNAME, SMSAPI_PASSWORD, SOUND_PLAYER_CMD_LINE

import smtplib
from email.mime.text import MIMEText

from smsapi.client import SmsAPI
from smsapi.responses import ApiError

import os
from threading import Thread


class Notifications:

	@staticmethod
	def send_email(recipient, subject, message):
		msg = MIMEText(message)
		msg['Subject'] = subject
		msg['From'] = MAIL_FROM
		msg['To'] = recipient
		
		smtp = smtplib.SMTP(MAIL_SERVER, '587')
		is_succeeded = False
		try:
			smtp.ehlo()
			if smtp.has_extn('STARTTLS'):
				smtp.starttls()
				smtp.ehlo()
			smtp.login(MAIL_LOGINNAME, MAIL_PASSWORD)
			smtp.sendmail(MAIL_FROM, recipient, msg.as_string())
			is_succeeded = True
		except Exception as e:
			print("send_email() exception: %s" % str(e))
		finally:
			smtp.quit()
		return is_succeeded
	
	@staticmethod
	def play_sound(filename, in_other_thread = True):
		play = lambda: os.popen(SOUND_PLAYER_CMD_LINE.replace('${FILENAME}',filename))
		if in_other_thread:
			Thread(name='play_sound() thread', target=play).start()
		else:
			play()
	
	@staticmethod
	def send_sms(recipient, message):
		sms = SmsAPI()
		sms.set_username(SMSAPI_LOGINNAME)
		sms.set_password(SMSAPI_PASSWORD)
		
		try:
			sms.service('sms').action('send')
			
			sms.set_content(message)
			sms.set_to(recipient)
			sms.set_eco(1)
			
			result = sms.execute()
			print("send_sms(): results: ")
			for r in result:
				print "ID=%s PNT_COST=%s STATUS=%s" % (str(r.id), str(r.points), r.status)
				
		except ApiError, e:
			print 'send_sms(): error: %s - %s' % (e.code, e.message)
	
	@staticmethod
	def call_phone(recipient, record_filename):
		print("call_phone(): sorry, not implemented yet")

	@staticmethod
	def log_to_stdout(message):
		print(message)