<html>
	<head>
		<title>HMC5883L Control Panel</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<link rel="stylesheet" type="text/css" href="/assets/css/styles.css" media="screen" />
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css" media="screen" />
		<link rel="stylesheet" href="/assets/css/bootstrap-theme.min.css" media="screen" />
	</head>
	<body>
		<div class="container">
			<div class="page-header">
				<h1>Live data:</h1>
			</div>
			Sensor status: <span id="device_status">Unknown</span>
			<div id="last_alarm_div">Last alarm at: <span id="last_alarm_info">Unknown</span></div>
			% if len(errors) > 0:
				<br /><br />
				<div class="alert alert-danger">
					<h4>Errors:</h4>
					<ul>
					% for error in errors:
						<li>{{error}}</li>
					% end
					</ul>
				</div>
			% end
		</div>
			<div id="chart_div"></div>
		<div class="container">
			

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3>Client configuration:</h3>
				</div>
				<div class="panel-body">
					% # TODO move these input fields to server configuration file
					Data refresh interval: <input id="refresh_interval_input" type="number" step="100" min="1" value="1000"> [msec] <br />
					<input id="use_default_scale_input" type="checkbox" checked> Use default chart scale <br />
					<input id="use_alert_input" type="checkbox"> Use alert() to notify alarm. <br />
					<br />
					<input id="applying_button" type="button" value="Apply" onclick="updateClientSettings();">
				</div>
			</div>
			

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3>Server configuration:</h3>
				</div>
				<div class="panel-body">
					<form id="server_conf_form" action="/" method="post">
						Measumerent frequency:
						<select name="sample_rate_mode" form="server_conf_form">
						% for mode in rate_modes:
							<option value="{{mode}}"{{' selected=selected' if sample_rate_mode == mode else ''}}>{{rate_modes[mode]}} Hz</option>
						% end
						</select>
						<hr class="spacer" />

						Measumerent history length: <input name="history_length" value="{{history_length}}" type="number" step="any" min="1" /> [sec]
						<hr class="spacer" />

						Sensor field range:
						<select id="gain_scale" name="gain_scale" form="server_conf_form">
						% for scale in gain_scales:
							<option value="{{scale}}"{{' selected=selected' if gain_scale == scale else ''}}>± {{gain_scales[scale]}} Gs</option>
						% end
						</select>

						<br />
						<br />
						<input value="Restart service" type="submit"/>
					</form>
				</div>
			</div>
			

			<script type="text/javascript" src="/assets/js/status_updater.js">
				// must be called after loading the whole document
			</script>
		</div>
	</body>
</html>