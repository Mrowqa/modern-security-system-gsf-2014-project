#!/bin/bash


cd $(dirname $0)
if [ "$1" == "-redirected-output" ]; then
	echo -n "$(date +%d/%b/%Y\ %X): Preparing enviroment... "
	. ./prepare_env.sh
	echo Done.

	echo "$(date +%d/%b/%Y\ %X): Starting service."
	exec ./http_server.py
else
	LOGFILE=$1
	if [ "$LOGFILE" != "" ]; then
		exec $0 -redirected-output >> $LOGFILE 2>&1
	else
		exec $0 -redirected-output
	fi
fi
