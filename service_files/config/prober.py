FREQUENCY_SAMPLE_RATE=4
# Initial frequency:
#	0 -> 0.75	Hz
#	1 -> 1.5	Hz
#	2 -> 3		Hz
#	3 -> 7.5	Hz
#	4 -> 15		Hz
#	5 -> 30		Hz
#	6 -> 75		Hz

GAIN_SCALE=1
# Initial gain scale (recommended scale / step,sensitivity):
#	0 -> 0.88 Gs / 0.73 mGs
#	1 -> 1.30 Gs / 0.92 mGs
#	2 -> 1.90 Gs / 1.22 mGs
#	3 -> 2.50 Gs / 1.52 mGs
#	4 -> 4.00 Gs / 2.27 mGs
#	5 -> 4.70 Gs / 2.56 mGs
#	6 -> 5.60 Gs / 3.03 mGs
#	7 -> 8.10 Gs / 4.35 mGs

MEASUREMENT_HISTORY_LENGTH=20
# Initial measurement history:
#	(number >= 1) sec


#SHOW_DIFFERENCIAL_DATA
# not implemented analyzer algorithm in this mode
# (chart in web panel shows good data), implicated always True


DISCONECTED_SENSOR_TRIGGERS_ALARM = True
# When sensor is disconected, then alarm is triggered.
#	(True|False) boolean value


ALARM_COOLDOWN=10
# After each alarm the data analyzer algorithm cannot trigger new until cooldown is missed
#	(number) sec
